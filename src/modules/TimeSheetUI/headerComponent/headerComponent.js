import { LightningElement } from 'lwc';
export default class HeaderComponent extends LightningElement {
    constructor() {
        super();
        const styles = document.createElement('link');
        styles.href = 'assets/styles/salesforce-lightning-design-system.css';
        styles.rel = 'stylesheet';
        this.template.appendChild(styles);
    }
}