import { LightningElement } from 'lwc';
export default class TimeSheetFormComponent extends LightningElement {
    constructor() {
        super();
        const styles = document.createElement('link');
        styles.href = 'assets/styles/salesforce-lightning-design-system.css';
        styles.rel = 'stylesheet';
        this.template.appendChild(styles);
    }
    openModal() {    
        // to open modal window set 'bShowModal' tarck value as true
        this.bShowModal = true;
    }
 
    closeModal() {    
        // to close modal window set 'bShowModal' tarck value as false
        this.bShowModal = false;
    }
}