import { buildCustomElementConstructor } from 'lwc';
import MyApp from 'my/app';
import TimeSheetFormComponent from 'TimeSheetUI/timeSheetFormComponent';
//const app = createElement('vaish-app', { is: MyApp });
// eslint-disable-next-line @lwc/lwc/no-document-query
//document.querySelector('#main').appendChild(app);
customElements.define('my-app', buildCustomElementConstructor(MyApp));
customElements.define('time-sheet-form-component', buildCustomElementConstructor(TimeSheetFormComponent));
